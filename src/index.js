const cors = require('cors');
const express = require('express');
const path = require('path');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

// Routes
const tasksRoutes = require('./routes/tasks');
//const usersRoutes = require('./routes/users');

// Init App
const app = express();

// Settings
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Routes
app.use('/api', tasksRoutes);
//app.use('/api', usersRoutes);

// Static Files
app.use(express.static(path.join(__dirname, '/dist/client')));

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

// Passport Init
app.use(passport.initialize());
app.use(passport.session());

// Start Server
app.listen(app.get('port'), () => {
    console.log('Server starts on port', app.get('port'));
});