const router = require('express').Router();
const mongojs = require('mongojs');
const db = mongojs('mongodb://scortes:1234@ds217970.mlab.com:17970/tasks_system', ['users']);
const Joi = require('joi');

// Insertar un USER
router.post('/users', (req, res, next) => {
    const { error } = validateUser(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    var emailInUse = false;

    db.users.findOne({email: req.body.email}, (err, user) => {
        if (err) 
        
    });

    const user = req.body;
    db.users.save(user, (err, user) => {
        if (err) return next(err);
        res.json(user);
    });
});

// Validar una tarea
function validateUser(user) {
    const schema = {
        _id: Joi.optional(),
        email: Joi.email().required(),
        passwd: Joi.string().required(),
        reg_date: Joi.optional()
    };

    return Joi.validate(user, schema);
}