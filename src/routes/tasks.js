const router = require('express').Router();
const mongojs = require('mongojs');
const db = mongojs('mongodb://scortes:1234@ds217970.mlab.com:17970/tasks_system', ['tasks']);
const Joi = require('joi');

// Obtener todos los TASKS
router.get('/tasks', (req, res, next) => {
    db.tasks.find((err, tasks) => {
        if (err) return next(err);
        res.json(tasks);
    });
});

// Obtener un TASK por su ID
router.get('/tasks/:id', (req, res, next) => {
    db.tasks.findOne({_id: mongojs.ObjectId(req.param.id)}, (err, task) => {
        if (err) return next(err);
        res.json(task);
    });
});

// Insertar un TASK
router.post('/tasks', (req, res, next) => {
    const { error } = validateTask(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const task = req.body;
    db.tasks.save(task, (err, task) => {
        if (err) return next(err);
        res.json(task);
    });
});

// Borrar un TASK
router.delete('/tasks/:id', (req, res, next) => {
    db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, (err, task) => {
        if (err) return next(err);
        res.json(task);
    });
})

// Actualizar un TASK
router.put('/tasks/:id', (req, res, next) => {
    const { error } = validateTask(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const updateTask = {
        title: req.body.title,
        isDone: req.body.isDone
    }
    db.tasks.update({_id: mongojs.ObjectId(req.params.id)}, updateTask, {}, (err, task) => {
        if (err) return next(err);
        res.json(task);
    });
});

// Validar una tarea
function validateTask(task) {
    const schema = {
        _id: Joi.optional(),
        title: Joi.string().required(),
        isDone: Joi.boolean().required()
    };

    return Joi.validate(task, schema);
}

module.exports = router;