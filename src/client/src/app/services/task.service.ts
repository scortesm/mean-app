import { Task } from './../Task';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TaskService {

	url: string = "http://localhost:3000/";

	constructor(private _http: HttpClient) { }

	// GET all Tasks
	getTasks(): Observable<Task[]> {
		const url = `${this.url}api/tasks`;
		return this._http.get<Task[]>(url);
	}

	// INSERT a Task
	addTask(task: Task): Observable<Task> {
		const url = `${this.url}api/tasks`;
		return this._http.post<Task>(url, task);
	}

	// DELETE a Task
	deleteTask(id: string): Observable<Task> {
		const url = `${this.url}api/tasks/${id}`;
		return this._http.delete<Task>(url);
	}

	// UPDATE a Task
	updateTask(task: Task): Observable<Task> {
		const url = `${this.url}api/tasks/${task._id}`;
		return this._http.put<Task>(url, task);
	}

}
